from rest_framework.serializers import ModelSerializer

from todo_back.models import Profile
from .models import Todo


class TodoSerializers(ModelSerializer):
    class Meta:
        model = Todo
        fields = ('id', 'title', 'tag', 'completed')

    def create(self, validated_data):
        profile = Profile.objects.get(user=self.context['request'].user)
        print(profile)
        todo = Todo.objects.create(**validated_data, owner=profile)
        return todo
