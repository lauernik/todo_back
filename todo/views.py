from rest_framework.generics import ListCreateAPIView, DestroyAPIView, UpdateAPIView
from .models import Todo
from todo_back.models import Profile
from .serializers import TodoSerializers
from rest_framework.permissions import IsAuthenticated


# Create your views here.
class TodoListView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = TodoSerializers

    def get_queryset(self):
        user = self.request.user
        profile = Profile.objects.get(user=user)
        return Todo.objects.filter(owner=profile)


class TodoDestroyView(DestroyAPIView, UpdateAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializers
