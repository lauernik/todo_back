from django.db import models
from todo_back.models import Profile


# Create your models here.
class Todo(models.Model):
    title = models.CharField(max_length=100)
    tag = models.CharField(max_length=50, blank=True)
    completed = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(Profile, on_delete=models.CASCADE)

    def __str__(self):
        return f'Todo: {self.title}'
