from django.urls import path
from .views import TodoListView, TodoDestroyView

urlpatterns = [
    path('', TodoListView.as_view()),
    path('<pk>', TodoDestroyView.as_view()),
]