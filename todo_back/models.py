from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'Profile: {self.user.username}'


@receiver(post_save, sender=User)
def create_profile(sender, instance, created=None, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def create_token(sender, instance, created=None, **kwargs):
    if created:
        Token.objects.create(user=instance)
