from django.contrib import admin
from todo_back.models import Profile

# Register your models here.
admin.site.register(Profile)
